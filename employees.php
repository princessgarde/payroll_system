<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Infosoft Studio</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">

	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

</head>
<body>
	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid" style="  background-color: #461d4c;">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span></button>
				<a class="navbar-brand" href="index.php" style="font-size:17px; color:white;"><b>Infosoft &nbsp;Studio</b></a>
			</div>
		</div>
	</nav>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
    <div class="profile-sidebar">
			<div class="profile-userpic">
				<img src="images.png" class="img-responsive" alt="">
			</div>
			<div class="profile-usertitle">
				<div class="profile-usertitle-name">Admin</div>
				<div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
			</div>
			<div class="clear"></div>
		</div>
		<ul class="nav menu">
			<li><a href="index.php"><em class="fa fa-home">&nbsp;</em> Home</a></li>
			<li><a href="employees.php" class="active1"><em class="fa fa-group">&nbsp;</em> Employees</a></li>
			<li><a href="settings.php"><em class="fa fa-gear">&nbsp;</em> Settings</a></li>
			<li><a href="login.php"><em class="fa fa-sign-out">&nbsp;</em> Logout</a></li>
		</ul>
	</div>
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="index.php">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Employees</li>
			</ol>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
                <br><br>
			</div>
		</div>
		
		
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading"style="color:black;">
						<div class="input-group" style=" margin-top: 20px;">
							<input id="btn-input" type="text" class="form-control input-md" placeholder="Search" /><span class="input-group-btn">
								<button class="btn btn-primary btn-md" id="btn-todo"><i class="fa fa-search"></i></button>
						</span></div>
					</div> 
					<div class="canvas-wrapper" style="height:540px">
                        <div class="container scrollable" style="font-size:14px">  
							   <br>
							<a href="#" class="btn-sm btn-success" role="button"><i class="fa fa-plus-circle"></i>&nbsp;Add New</a>
							<br>
                            <table class="table table-hover table-striped">
                                <thead>
                                        <tr>
                                            <th>ID Number</th>
                                            <th>Full Name</th>
                                            <th>SSS Number</th>
                                            <th>Tin Number</th>
                                            <th>Position</th>
                                            <th>Date Hired</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>IS12345</td>
                                            <td class="name-link" style="cursor:pointer" onclick="location.href='e1.php'">Juan A. Dela Cruz</td>
                                            <td>12-3456789-0</td>
                                            <td>201293478532</td>
                                            <td>Project Manager</td>
                                            <td>October 20, 2008</td>
                                            <td>
                                                <div class="btn-group btn-group-xs">
                                                    <a href="#" class="btn btn-primary" role="button"  style="width:72px;"><i class="fa fa-edit"></i>&nbsp;Edit</a>
                                                    <a href="#" class="btn btn-danger" role="button"><i class="fa fa-eraser"></i>&nbsp;Delete</a>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                        <td>IS12346</td>
                                            <td class="name-link" style="cursor:pointer" onclick="location.href='e2.php'">Julia B. Dela Torre</td>
                                            <td>12-3226789-8</td>
                                            <td>201543278347</td>
                                            <td>Developer</td>
                                            <td>January 30, 2013</td>
                                            <td>
                                                <div class="btn-group btn-group-xs">
                                                    <a href="#" class="btn btn-primary" role="button"  style="width:72px;"><i class="fa fa-edit"></i>&nbsp;Edit</a>
                                                    <a href="#" class="btn btn-danger" role="button"><i class="fa fa-eraser"></i>&nbsp;Delete</a>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                        <td>IS12347</td>
                                            <td class="name-link" style="cursor:pointer" onclick="location.href='e3.php'">Peter C. Cruz Jr.</td>
                                            <td>12-3456734-1</td>
                                            <td>201223459345</td>
                                            <td>UI / UX Designer</td>
                                            <td>May 7, 2011</td>
                                            <td>
                                                <div class="btn-group btn-group-xs">
                                                    <a href="#" class="btn btn-primary" role="button"  style="width:72px;"><i class="fa fa-edit"></i>&nbsp;Edit</a>
                                                    <a href="#" class="btn btn-danger" role="button"><i class="fa fa-eraser"></i>&nbsp;Delete</a>
                                                </div>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>	
    </div>	
	<nav class="navbar-inverse navbar-fixed-bottom" style="background-color:#461d4c; height:10px;"></nav>
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
	<script>
		window.onload = function () {
	var chart1 = document.getElementById("line-chart").getContext("2d");
	window.myLine = new Chart(chart1).Line(lineChartData, {
	responsive: true,
	scaleLineColor: "rgba(0,0,0,.2)",
	scaleGridLineColor: "rgba(0,0,0,.05)",
	scaleFontColor: "#c5c7cc"
	});
    };
	</script>
		
</body>
</html>