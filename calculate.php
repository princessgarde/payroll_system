<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Infosoft Studio</title>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/datepicker3.css" rel="stylesheet">
	<link href="css/styles.css" rel="stylesheet">

	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

</head>
<body>

	<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
		<div class="container-fluid" style="  background-color: #461d4c;">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span></button>
				<a class="navbar-brand" href="index.php" style="font-size:17px; color:white;"><b>Infosoft &nbsp;Studio</b></a>
			</div>
		</div>
	</nav>
	<div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
    <div class="profile-sidebar">
			<div class="profile-userpic">
				<img src="images.png" class="img-responsive" alt="">
			</div>
			<div class="profile-usertitle">
				<div class="profile-usertitle-name">Admin</div>
				<div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
			</div>
			<div class="clear"></div>
		</div>
		<ul class="nav menu">
			<li><a href="index.php"><em class="fa fa-home">&nbsp;</em> Home</a></li>
			<li><a href="employees.php" class="active1"><em class="fa fa-group">&nbsp;</em> Employees</a></li>
			<li><a href="settings.php"><em class="fa fa-gear">&nbsp;</em> Settings</a></li>
			<li><a href="login.php"><em class="fa fa-sign-out">&nbsp;</em> Logout</a></li>
		</ul>
	</div>
		
	<div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">
		<div class="row">
			<ol class="breadcrumb">
				<li><a href="index.php">
					<em class="fa fa-home"></em>
				</a></li>
				<li class="active">Employees</li>
			</ol>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
                <br><br>
			</div>
		</div>
		
		
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="panel-heading" style="height:600px; color:black">
						<div class="canvas-wrapper scrollable">
                        <div class="cont">
<div>

<?php
 error_reporting(0);
	$emp_id = trim($_REQUEST['emp_id']);
	$emp_name = trim($_REQUEST['emp_name']);
	$emp_position = trim($_REQUEST['emp_position']);
	$emp_salary = trim($_REQUEST['emp_salary']);
	$latehours = trim($_REQUEST['latehours']);
	$absentdays = trim($_REQUEST['absentdays']);
	$halfdays = trim($_REQUEST['halfdays']);
	$ot = trim($_REQUEST['ot']);
	$ca = trim($_REQUEST['ca']);
	$reg_holiday_hour = trim($_REQUEST['reg_holiday_hour']);
	$reg_holiday_ot = trim($_REQUEST['reg_holiday_ot']);
	$sp_holiday_hour = trim($_REQUEST['sp_holiday_hour']);
	$sp_holiday_ot = trim($_REQUEST['sp_holiday_ot']);

 // Clear the text field

 if ($_REQUEST['submit']){
	$emp_name = "";
	$emp_position = "";
	$emp_id ="";
	$emp_salary = "";
	$phil_health = "";
	$pag_ibig = "";
	$absentdays ="";
	$reg_holiday_hour = "";
	$reg_holiday_ot = "";
	$sp_holiday_hour = "";
	$sp_holiday_ot = "";
	$halfdays ="";
	$ca = "";
	$latehours="";
	$ot = "";

}


// Here To Perform Computation of the Payroll
if ($_REQUEST['submit2']){
	$phil_health = ($emp_salary * 0.01375);
	 $pag_ibig = ($emp_salary * 0.01);
	 $sss = ($emp_salary * 0.01815);
	 $perday = ($emp_salary / 11.5);
	$perhour = (($emp_salary / 11.5) / 8);
	$half = (($emp_salary / 11.5) / 2);;
	$reg_hol_ot_add = (($reg_holiday_ot * $perhour) * 0.30 );
	$reg_hol_ot = (($reg_holiday_ot * $perhour) + $reg_hol_ot_add);
	$reg_hol_hrs = (($reg_holiday_hour * $perhour) * 2);
	$sp_hol_hour_add = (($sp_holiday_hour * $perhour) * 0.30 );
	$sp_hol_hour = (($sp_holiday_hour * $perhour) + $sp_hol_hour_add);
	$sp_hol_ot_add = (($sp_holiday_ot * $perhour) * 0.30 );
	$sp_hol_ot = (($sp_holiday_ot * $perhour) + $sp_hol_ot_add);
	$absent = ($perday * $absentdays);
    $halfday = ($half * $halfdays);
    $late =($perhour * $latehours);
    $overtime = ($perhour * $ot);
    $regular_holiday = ($reg_hol_hrs + $reg_hol_ot);
    $special_holiday = ($sp_hol_hour + $sp_hol_ot);
	$add_deductions = ( $absent + $halfday + $late + $sss + $phil_health + $pag_ibig + $ca);
	$add_earnings = ( $overtime  + $reg_hol_hrs + $reg_hol_ot + $sp_hol_hour + $sp_hol_ot);
	$compute = (($emp_salary + $add_earnings) - $add_deductions);
	$total_deductions = number_format($add_deductions,2,'.',',');
	$total_earnings = number_format($add_earnings,2,'.',',');
	$net_pay = number_format($compute,2,'.',',');
	$emp_salary = number_format($emp_salary,2,'.',',');
}
?>


<form method="post" action="">
<div>
	<br>
<div class="form-group" style="font-size:14px;">
    <label for="date">Date:</label>
    <ip><?php echo  date("F d, Y");?></p>
  </div>
<div class="form-group form-horizontal row">
      <div class="col-xs-3" style="min-width:200px;">
        <label style="font-size:12px" for="emp_id">ID NO.</label>
        <input name="emp_id" class="form-control" style="background-color:white; height:25px;" id="ex1" type="text" value="<?php echo $emp_id; ?>">
      </div>
      <div class="col-xs-4" style="min-width:250px;">
        <label  style="font-size:12px"  for="emp_name">FULL NAME</label>
        <input name="emp_name" class="form-control" style="background-color:white; height:25px;" id="ex2" type="text" value="<?php echo $emp_name; ?>">
      </div>
      <div class="col-xs-4" style="min-width:250px;">
        <label  style="font-size:12px"   for="emp_position">Position</label>
        <input name="emp_position" class="form-control" style="background-color:white; height:25px;" id="ex3" type="text" value="<?php echo $emp_position; ?>">
	  </div>
	  

 </div>

 <hr>



 <div class="panel-body">
 <form class="form-inline" action="/action_page.php">
 <div class="form-group">
									<label style="font-size:14px; text-align:left" class="col-md-2 control-label" for="emp_salary">Employee's Salary:</label>
									<div class="col-md-9">
										<input name="emp_salary" style="width:30%; height:30px; background-color:white;"type="text" class="form-control" value="<?php echo $emp_salary; ?>">
									</div>
									<br><br>
 <label style="font-size:14px">TOTAL Earnings</label> <br>
						<form class="form-horizontal" action="" method="post">
							<fieldset>

								<div class="form-group">
									<label style="font-size:14px; text-align:right" class="col-md-2 control-label" for="ot">Overtime Hours</label>
									<div class="col-md-9">
										<input name="ot" style="width:30%; height:30px;"type="text" placeholder="Enter overtime hours..." class="form-control mid" value="<?php echo $ot; ?>">
									</div>
								</div>
								<br>
								<div class="form-group">
									<label style="font-size:14px; text-align:right" class="col-md-2 control-label">Regular Holidays</label>
									<div class="col-md-9">
										<hr>
								</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" style="font-size:12px; text-align:right;" for="reg_holiday_hour">Hours</label>
									<div class="col-md-9">
										<input name="reg_holiday_hour" type="text"  style="width:30%; height:30px;" placeholder="Enter hours..." class="form-control" value="<?php echo $reg_holiday_hour; ?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" style="font-size:12px; text-align:right"  for="reg_holiday_ot">Overtime</label>
									<div class="col-md-9">
										<input name="reg_holiday_ot" type="text" style="width:30%; height:30px;" placeholder="Enter hours..." class="form-control" value="<?php echo $reg_holiday_ot; ?>">
									</div>
								</div>
								
								<div class="form-group">
									<label style="font-size:14px; text-align:right" class="col-md-2 control-label">Special Holidays</label>
									<div class="col-md-9">
										<hr>
								</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" style="font-size:12px; text-align:right;" for="sp_holiday_hour">Hours</label>
									<div class="col-md-9">
										<input name="sp_holiday_hour" type="text"  style="width:30%; height:30px;" placeholder="Enter hours..." class="form-control" value="<?php echo $sp_holiday_hour; ?>">
									</div>
								</div>
								<div class="form-group">
									<label class="col-md-2 control-label" style="font-size:12px; text-align:right"  for="sp_holiday_ot">Overtime</label>
									<div class="col-md-9">
										<input name="sp_holiday_ot" type="text" style="width:30%; height:30px;" placeholder="Enter hours..." class="form-control" value="<?php echo $sp_holiday_ot; ?>">
									</div>
								</div>
	
				<label style="font-size:14px">TOTAL Deductions</label> <br>
								<div class="form-group">
									<label style="font-size:14px; text-align:right" class="col-md-2 control-label" for="absentdays">Absences</label>
									<div class="col-md-9">
										<input name="absentdays" style="width:30%; height:30px;"type="text" placeholder="Number of days absent..." class="form-control" value="<?php echo $absentdays; ?>">
									</div>
								</div>
								<div class="form-group">
									<label style="font-size:14px; text-align:right" class="col-md-2 control-label" for="halfdays">Half Days</label>
									<div class="col-md-9">
										<input name="halfdays" style="width:30%; height:30px;"type="text" placeholder="Number of half days..." class="form-control" value="<?php echo $halfdays; ?>">
									</div>
								</div>
								<div class="form-group">
									<label style="font-size:14px; text-align:right" class="col-md-2 control-label" for="latehours">Lates</label>
									<div class="col-md-9">
										<input name="latehours" style="width:30%; height:30px;"type="text" placeholder="Enter late hours..." class="form-control" value="<?php echo $latehours; ?>">
									</div>
								</div>
								<div class="form-group">
									<label style="font-size:14px; text-align:right" class="col-md-2 control-label" for="ca">Cash Advance</label>
									<div class="col-md-9">
										<input name="ca" style="width:30%; height:30px;"type="text" placeholder="Enter total cash advance..." class="form-control" value="<?php echo $ca; ?>">
									</div>
								</div>
								<div class="form-group">
									<label style="font-size:14px; text-align:right" class="col-md-2 control-label" for="sss">SSS</label>
									<div class="col-md-9">
										<input name="sss" style="width:30%; height:30px; background-color:white;"type="text" class="form-control" value="<?php echo $sss; ?>" readonly>
									</div>
								</div>
								<div class="form-group">
									<label style="font-size:14px; text-align:right" class="col-md-2 control-label" for="pag_ibig">PAG-IBIG</label>
									<div class="col-md-9">
										<input name="pag_ibig" style="width:30%; height:30px; background-color:white;"type="text" class="form-control" value="<?php echo $pag_ibig; ?>" readonly>
									</div>
								</div>
								<div class="form-group">
									<label style="font-size:14px; text-align:right" class="col-md-2 control-label" for="phil_health">PHILHEALTH</label>
									<div class="col-md-9">
										<input name="phil_health" style="width:30%; height:30px; background-color:white;"type="text" class="form-control" value="<?php echo $phil_health; ?>" readonly>
									</div>
								</div>
								<!-- Form actions -->
									<div>
										<input  style="margin-top:20px" type="submit" name="submit2" value="Compute Salary" class="btn btn-primary btn-xs" title="Click here to compute the salary.">
										<input style="margin-top:20px"  type="submit" name="submit" value="Clear" class="btn btn-default btn-xs" title="Click here to clear the textbox."	>
									</div>
									<hr style="display: block; height:1px; border: 0; border-top: 3px solid #ccc;margin: 1em 0;padding: 0;">
								
								</div>
								<div class="form-group">
									<label style="font-size:14px; text-align:right" class="col-md-2 control-label" for="total_earnings">Total Earnings:</label>
									<div class="col-md-9">
										<input name="total_earnings" style="width:30%; height:30px; background-color:white;"type="text" class="form-control" value="<?php echo $total_earnings; ?>" readonly>
									</div>
								</div>
								<div class="form-group">
									<label style="font-size:14px; text-align:right" class="col-md-2 control-label" for="total_deductions">Total Deductions:</label>
									<div class="col-md-9">
										<input name="total_deductions" style="width:30%; height:30px; background-color:white;"type="text" class="form-control" value="<?php echo $total_deductions; ?>" readonly>
									</div>
								</div>
								<div class="form-group">
									<label style="font-size:14px; text-align:right" class="col-md-2 control-label" for="net_pay">Net Pay:</label>
									<div class="col-md-9">
										<input name="net_pay" style="width:30%; height:30px; background-color:white;"type="text" class="form-control" value="<?php echo $net_pay; ?>" readonly>
									</div>
								</div>
								</div>
							</fieldset>
						</form>
						<br><br><br><br>
					</div>
 

<script>
$(window).resize(function() {
	var path = $(this);
	var contW = path.width();
	if(contW >= 751){
		document.getElementsByClassName("sidebar-toggle")[0].style.left="200px";
	}else{
		document.getElementsByClassName("sidebar-toggle")[0].style.left="-200px";
	}
});
$(document).ready(function() {
	$('.dropdown').on('show.bs.dropdown', function(e){
	    $(this).find('.dropdown-menu').first().stop(true, true).slideDown(300);
	});
	$('.dropdown').on('hide.bs.dropdown', function(e){
		$(this).find('.dropdown-menu').first().stop(true, true).slideUp(300);
	});
	$("#menu-toggle").click(function(e) {
		e.preventDefault();
		var elem = document.getElementById("sidebar-wrapper");
		left = window.getComputedStyle(elem,null).getPropertyValue("left");
		if(left == "200px"){
			document.getElementsByClassName("sidebar-toggle")[0].style.left="-200px";
		}
		else if(left == "-200px"){
			document.getElementsByClassName("sidebar-toggle")[0].style.left="200px";
		}
	});
});</script>

                        </div>
					</div>
				</div>
			</div>
		</div>
		<nav class="navbar-inverse navbar-fixed-bottom" style="background-color:#461d4c; height:10px;"></nav>
	
	<script src="js/jquery-1.11.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/chart.min.js"></script>
	<script src="js/chart-data.js"></script>
	<script src="js/easypiechart.js"></script>
	<script src="js/easypiechart-data.js"></script>
	<script src="js/bootstrap-datepicker.js"></script>
	<script src="js/custom.js"></script>
	<script>
		window.onload = function () {
	var chart1 = document.getElementById("line-chart").getContext("2d");
	window.myLine = new Chart(chart1).Line(lineChartData, {
	responsive: true,
	scaleLineColor: "rgba(0,0,0,.2)",
	scaleGridLineColor: "rgba(0,0,0,.05)",
	scaleFontColor: "#c5c7cc"
	});
};
	</script>
	
</body>
</html>